//
//  ViewController.swift
//  RxSwiftTest
//
//  Created by deng on 2020/9/2.
//  Copyright © 2020 deng. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class ViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let result = textField.rx.text.orEmpty
            .asDriver()
            .flatMap {
                return self.dealData(inputText: $0)
                    .asDriver(onErrorJustReturn: "检测到了错误事件")
            }
        
        // 第一次订阅
        result.map { "按钮显示的标题长度：\(($0).count)" }
            .drive(label.rx.text)
            .disposed(by: disposeBag)
        
        // 第二次订阅
        result.map { $0 }
            .drive(button.rx.title())
            .disposed(by: disposeBag)
    }
 
    // 模拟网络请求代码
    func dealData(inputText: String) -> Observable<String> {
        print("请求网络了 \(Thread.current)")
        return Observable<String>.create({ (observer) -> Disposable in
            // 模拟发出错误
            if inputText == "1234" {
                observer.onError(NSError(domain: "deng.com", code: 10086, userInfo: nil))
            }
            
            // 异步发送信号
            DispatchQueue.global().async {
                print("发送之前看看：\(Thread.current)")
                observer.onNext("已经输入：" + inputText)
                observer.onCompleted()
            }
            return Disposables.create()
        })
    }
}



