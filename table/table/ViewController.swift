//
//  ViewController.swift
//  table
//
//  Created by deng on 2020/8/29.
//  Copyright © 2020 deng. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UITableViewController {

    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var dataSoure = [String]()
        dataSoure.append("小哥哥")
        dataSoure.append("小姐姐")
        dataSoure.append("大叔")
        dataSoure.append("小萝莉")
    
        // 初始化数据
        let items = Observable.just(dataSoure)
        
        // 设置单元格数据（其实就是对 cellForRowAt 的封装）
        items.bind(to: self.tableView.rx.items) {
            (tableView, row, element) in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.accessoryType = .detailDisclosureButton
            cell?.textLabel?.text = "\(row): \(element)"
            return cell!
        }.disposed(by: disposeBag)
        
//        // 获取点击行
//        self.tableView.rx.itemSelected.subscribe(onNext: { (index) in
//            print("\(index.row)")
//        }).disposed(by: disposeBag)
//        
    }

}

